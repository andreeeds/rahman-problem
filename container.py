import random

from threading import Lock

__all__ = [ 'Container' ]

class Container(object):
  '''
  Container detail information

  Attributes:
    __containerLock: Lock - mutex for container access
    __container: dict - rahman's ball containers
    __capacity: int - maximum container capacity
  '''
  def __init__(self, capacity):
    self.__containerLock = Lock()
    self.reset(capacity)

  def reset(self, capacity):
    with self.__containerLock:
      self.__capacity = capacity
      self.__container = {}

  def put(self, containerNumber):
    '''
    Put one ball into container

    Args:
      containerNumber: int - container number will be put one ball

    Return:
      boolean: True if container is full after put one ball

    Raises:
      ValueError: if one container already full
    '''
    if self.isVerified():
      # cannot put if one container is full
      raise ValueError('One container already full')

    ret = False
    with self.__containerLock:
      if containerNumber in self.__container:
        self.__container[containerNumber] = self.__container[containerNumber] + 1
      else:
        self.__container[containerNumber] = 1

      ret = True if self.__container[containerNumber] == self.__capacity else False

    return ret

  def isVerified(self):
    '''
    Check container is full or not

    Return:
      boolean: True if one container is full
    '''
    ret = False
    with self.__containerLock:
      if len(self.__container) > 0:
        # check when at least one container filled with one ball
        sizes = list(self.__container.values())
        sizes.sort(reverse=True)
        ret = True if sizes[0] == self.__capacity else False

    return ret

if __name__ == '__main__':
  # define containers
  container = Container(5)

  # random putting ball
  while not container.isVerified():
    container.put(random.randint(1, 10000))

  container.put(random.randint(1, 10000))
